let myQuestions = [
    {
    question : 'De quelle ville suédoise les groupes Dark Tranquillity, In Flames, et HammerFall sont-ils originaires ?',
    a : 'Örebro',
    b : 'Stockholm',
    c : 'Göteborg',
    d : 'Malmö',
    correctAnswer : 'C'
    },
    {
    question : 'Quel est nom du 1er album du groupe "Bring me the Horizon" (BMTH) ?',
    a : 'Sempiternal',
    b : 'Suicide Season',
    c : 'Count Your Blessing',
    d : 'There is a Hell Believe. There Is a Heaven, Let\'s Keep It a Secret.',
    correctAnswer : 'C'
    },
    {
    question : 'Parmis ces groupes de Metalcore, lequel n\'est pas originaire de France ?',
    a : 'Novelists',
    b : 'Landmvrks',
    c : 'Resolve',
    d : 'The Brave',
    correctAnswer : 'D'
    },
    {
    question : 'Comment nomme-t-on la fusion entre le punk hardcore et le métal ?',
    a : 'Metalcore',
    b : 'Deathcore',
    c : 'Hard Punk',
    d : 'Black Metal',
    correctAnswer : 'A'
    },
    {
    question : 'Dans quelle décennie le métal a-t-il reçu le plus de succès auprès du grand public ?',
    a : '1970',
    b : '1980',
    c : '1990',
    d : '2000',
    correctAnswer : 'B'
    },
]

let questionsContainer = document.querySelector('#question-container');
let questionProgress = document.querySelector('#question-progress');
let currentQuestion = document.querySelector('#current-question');
let startBtn = document.querySelector('#start');
let nextBtn = document.querySelector('#next');
let finalPoint = document.querySelector('.endPoints')
let messageEnd = document.querySelector('.messageEnd')
let choice = document.querySelectorAll('.choice');
let answerA = document.querySelector('#answerA');
let answerB = document.querySelector('#answerB');
let answerC = document.querySelector('#answerC');
let answerD = document.querySelector('#answerD');
let totalScore = document.querySelector('#score')

let indexQuestion = 0;
let score = 0;

startBtn.style.display =  'block';

// Affichage de mes questions
function showQuestions() {
    let q = myQuestions[indexQuestion];
    currentQuestion.innerHTML = q.question;
    questionProgress.innerHTML = indexQuestion +1
    answerA.textContent = q.a;
    answerB.textContent = q.b;
    answerC.textContent = q.c;
    answerD.textContent = q.d;
}
showQuestions()

// Comparaison de la question choisi et la réponse correct
// Dans le cas ou la question est juste, je gagne 1 pt, puis je passe à la question suivante
// Dans le cas contraire, je passe à la question suivante sans gagner de point
for(let selectedAnswer of choice) {
    selectedAnswer.addEventListener('click', () => {
        if (indexQuestion < myQuestions.length -1){
            if (selectedAnswer.classList.contains(myQuestions[indexQuestion].correctAnswer)) {
                score++
                totalScore.innerHTML= score;
                indexQuestion++;
                showQuestions();
            }else{
                indexQuestion++;
                showQuestions();
            }         
        } else {
            if  (selectedAnswer.classList.contains(myQuestions[indexQuestion].correctAnswer)) {
                score++
                totalScore.innerHTML= score;
                messageEnd.style.display = 'block';
                finalPoint.textContent = score;          
            } else {
            messageEnd.style.display = 'block';
            finalPoint.textContent = score;  
        }
    }
    })
}

// Retire la page de start et affichage le Quiz
startBtn.addEventListener('click', () => {
    if (startBtn.style.display === 'block'){
        startBtn.style.display = 'none' ; 
        questionsContainer.style.display = 'block';
    } 
})



// Décompte fonctionnel, mais il se déclenche dès le chargement de la page 
// et ne se remets pas à 0 pour chaque question. 

// let count = 15;
// let interval = setInterval(function(){
//     document.querySelector('#timer').innerHTML=count;
//     count--;
//     if (count === 0){
//         clearInterval(interval);
//         document.querySelector('#timer').innerHTML='Terminé';
//         alert("Oh no, votre temps est écoulé");
//     }
// }, 1000);

