# Portfolio

## _Deuxième projet :_

Durant la formation Développement Web & Mobile, j'ai eu pour second projet la réalisation d'un Quiz du thême de notre choix.
Dans un premier temps j'ai dû réfléchir au thème et aux questions que je souhaitais y poser, puis y concevoir les maquettes
Nos règles étaient que l'on affiche les questions et les réponses puis qu'on les enchaînes (sans changement de page)
Et pour finir il devait afficher le compte des points tout en étant responsive

Ayant malheureusement eu des difficultés sur mon Quiz, le timer existe ,mais ayant quelques problèmes avec celui-ci, il est actuellement désactivé. De plus, l'aspect visuel est loin d'être aussi bons que dans mes attentes. Mais j'ai préférais privilégier l'aspect fonctionnel que visuel

## Caractéristiques

- Responsive
- Affichage des questions et des réponses sans changement de page
- Incrémentations des points en temps réels

### Maquettes

[Mes Maquettes](/public/design) \
[Lien du projet](https://andymez.gitlab.io/Quiz/)